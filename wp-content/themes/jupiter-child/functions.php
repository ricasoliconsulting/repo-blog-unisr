<?php

/* LOGIN CUSTOM */

function my_custom_login() {
    echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/login/rc-login-styles.css" />';
}
add_action('login_head', 'my_custom_login');

function login_error_override() {
    return 'Incorrect login details. If you need help contact us at <a href="mailto:info@ricasoliconsulting.com">info@ricasoliconsulting.com</a>';
}
add_filter('login_errors', 'login_error_override');

function my_login_head() {
	remove_action('login_head', 'wp_shake_js', 12);
}
add_action('login_head', 'my_login_head');

function my_login_logo_url() {
	return get_bloginfo( '#' );
}
add_filter( 'login_headerurl', 'my_login_logo_url' );


/* REDIRECT USER AFTER LOGIN */

function my_login_redirect( $url, $request, $user ){
    if( $user && is_object( $user ) && is_a( $user, 'WP_User' ) ) {
        if( $user->has_cap( 'editor' ) ) {
            $url = home_url('/wp-admin/edit.php');
        }
    }
    return $url;
}
add_filter('login_redirect', 'my_login_redirect', 10, 3 );


/* NAV CUSTOM */

function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'Custom Widget Navigation',
		'id'            => 'rc_widget_nav',
		'before_widget' => '<div class="rc-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rc-widget-title">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );


/* NEWS GRID FUNCTION FOR ADVANCED TEMPLATE */

add_filter('tg_register_item_skin', function($skins) {

    // just push your skin slugs (file name) inside the registered skin array
    $skins = array_merge($skins,
        array(
            'tg-newsnuova' => array(
                'filter'   => 'Personale', // filter name used in slider skin preview
                'name'     => 'News Nuova', // Skin name used in skin preview label
                'col'      => 1, // col number in preview skin mode
                'row'      => 1  // row number in preview skin mode
            )
        )
    );

    // return all skins + the new one we added (my-skin1)
    return $skins;

});
add_filter('tg_register_item_skin', function($skins) {

    // just push your skin slugs (file name) inside the registered skin array
    $skins = array_merge($skins,
        array(
            'tg-newsmod' => array(
                'filter'   => 'Personale', // filter name used in slider skin preview
                'name'     => 'News Modificata', // Skin name used in skin preview label
                'col'      => 1, // col number in preview skin mode
                'row'      => 1  // row number in preview skin mode
            )
        )
    );

    // return all skins + the new one we added (my-skin1)
    return $skins;

});
