<?php
/**
* @package   The_Grid
* @author    Themeone <themeone.master@gmail.com>
* @copyright 2015 Themeone
*
* Skin Name: News
* Skin Slug: tg-news
* Date: 05/02/17 - 10:15:53AM
*
*/

// Exit if accessed directly
if (!defined('ABSPATH')) {
	exit;
}

// Init The Grid Elements instance
$tg_el = The_Grid_Elements();

// Prepare main data for futur conditions
$image  = $tg_el->get_attachment_url();
$format = $tg_el->get_item_format();

$output = null;

$media = $tg_el->get_media();

// if there is a media
if ($media) {

	// Media wrapper start
	$output .= $tg_el->get_media_wrapper_start();

	// Media content (image, gallery, audio, video)
	$output .= $media;

	// if there is an image
	if ($image || in_array($format, array('gallery', 'video'))) {




		$output .= $tg_el->add_layer_action(array('action' => array('type' => 'link', 'link_target' => '_self', 'link_url' => 'post_url', 'custom_url' => '', 'meta_data_url' => '', 'position' => 'under')), 'tg-absolute');

	}

	$output .= $tg_el->get_media_wrapper_end();
	// Media wrapper end

}

// Bottom content wrapper start
$output .= $tg_el->get_content_wrapper_start('', 'bottom');
	$output .= $tg_el->get_the_title(array('link' => false, 'action' => array('type' => 'link', 'link_target' => '_self', 'link_url' => 'post_url', 'custom_url' => '', 'meta_data_url' => '')), 'tg-element-1');
	$output .= $tg_el->get_line_break();
	$output .= $tg_el->get_the_excerpt(array('length' => '214', 'suffix' => '...'), 'tg-element-3');
	$output .= $tg_el->get_the_date(array('format' => 'F j, Y'), 'tg-element-7');
	$output .= '<div class="mk-love-holder tg-element-4">';
	$output .= '<a href="#" class="mk-love-this item-loved" id="mk-love-'.$tg_el->get_item_ID().'">';
	$output .= '<svg class="mk-svg-icon sp-svg-icon" data-name="mk-icon-heart" data-cacheid="icon-58342265b7d7d" style=" height:16px; width: 16px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792"><path d="M896 1664q-26 0-44-18l-624-602q-10-8-27.5-26t-55.5-65.5-68-97.5-53.5-121-23.5-138q0-220 127-344t351-124q62 0 126.5 21.5t120 58 95.5 68.5 76 68q36-36 76-68t95.5-68.5 120-58 126.5-21.5q224 0 351 124t127 344q0 221-229 450l-623 600q-18 18-44 18z"></path></svg>';
	$output .= '<span class="sp-love-number">'.$tg_el->get_item_meta('_mk_post_love')."</span>";
	$output .= '</a>';
	$output .= '</div>';
	$output .= $tg_el->get_content_clear();
$output .= $tg_el->get_content_wrapper_end();
// Bottom content wrapper end

return $output;
