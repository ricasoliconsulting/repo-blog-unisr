<!-- Start of Post Wrap -->
<div class="post rc-editorial-wrapper">
    
    <!-- output the content -->
    <div class="rc-editorial mk-text-block">
        <?php the_content(); ?>
    </div>
    
    <div class="rc-clear"></div>
</div>
<!-- // End of Post Wrap -->
