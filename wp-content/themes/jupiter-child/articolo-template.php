<!-- Start of Post Wrap -->
<div class="post hentry ivycat-post rc-post-wrapper">
	
    <!-- This is the output of the post TITLE -->
	<h2 class="entry-title rc-post-title">
    <?php the_title(); ?>
    </h2>
    
    <!-- output the image -->
    <div class="rc-post-image">
        <?php the_post_thumbnail('full'); ?>
    </div>
    <div class="rc-line-sep"></div>
    
    <!-- output the content -->
    <div class="rc-post-content">
        <?php the_content(); ?>
    </div>
    
    <!-- Foot of the article -->
    <div class="rc-post-date rc-right">
        <?php the_date('F, Y'); ?>
    </div>
    <div class="rc-clear"></div>
    
    <div class="rc-post-comment rc-right rc-meta">
		<div class="rc-comment-holder">
		<?php echo mk_get_shortcode_view('mk_blog', 'components/comments', true); ?>
		</div>
    </div>
    
    <div class="rc-post-love rc-right rc-meta">
        <?php echo mk_get_shortcode_view('mk_blog', 'components/love-this', true); ?>
    </div>
    
    <div class="rc-post-share rc-right rc-meta">
        <?php echo mk_get_shortcode_view('mk_blog', 'components/social-share', true); ?>
    </div>
    <div class="rc-clear"></div>
    <div class="rc-line-sep last"></div>
</div>
<!-- // End of Post Wrap -->
